# Einfache Web-App in einem Container

## Image TWO
Dies ist eine einfach zusammengestrickte node.js-Anwendung, um den Einsatz von Microservices in Containern aufzuzeigen

Dieses Image wird **nicht** gepflegt und beinhaltet allfällige "Vulnerabilities". <br> 
Nutzung auf **eigenes Risiko**

Der Web-Dienst wird auf Port `8080` freigegeben - siehe `./app.js`

Weitere Details sind im `Dockerfile` ersichtlich